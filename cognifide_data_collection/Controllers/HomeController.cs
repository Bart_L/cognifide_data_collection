﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cognifide_data_collection.Models;
using cognifide_data_collection.ViewModels;

namespace cognifide_data_collection.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Customers
        public ViewResult Index(string sortOrder)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "birthdate_asc" ? "birthdate_desc" : "birthdate_asc";
            ViewBag.GenderSortParm = sortOrder == "gender_asc" ? "gender_desc" : "gender_asc";
            var users = from user in _context.Users select user;

            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(user => user.SecondName);
                    break;
                case "birthdate_asc":
                    users = users.OrderBy(user => user.Birthdate);
                    break;
                case "birthdate_desc":
                    users = users.OrderByDescending(user => user.Birthdate);
                    break;
                case "gender_asc":
                    users = users.OrderBy(user => user.Gender);
                    break;
                case "gender_desc":
                    users = users.OrderByDescending(user => user.Gender);
                    break;
                default:
                    users = users.OrderBy(user => user.SecondName);
                    break;
            }

            return View(users);
        }

        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(User user)
        {

            if (user.Id == 0)
            {
                _context.Users.Add(user);
            }
            else
            {
                var customerInDb = _context.Users.Single(c => c.Id == user.Id);

                // this aproach is not recommended because of security holes
                // TryUpdateModel(customerInDb);

                // instead of above, save manually
                customerInDb.Name = user.Name;
                customerInDb.Birthdate = user.Birthdate;
                customerInDb.SecondName = user.SecondName;
                customerInDb.Gender = user.Gender;
                customerInDb.Pesel = user.Pesel;

                // antoher approach using mapper, but with whole object
                // Mapper.Map(customer, customerInDb);
            }
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Edit(int id)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);

            if (user == null)
                return HttpNotFound();

            var viewModel = new UserFormViewModel
            {
                User = user
            };

            return View("Edit", viewModel);
        }
    }
}