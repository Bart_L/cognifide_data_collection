﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace cognifide_data_collection.Models
{
    public class User
    {

        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string SecondName { get; set; }

        [Required]
        public DateTime? Birthdate { get; set; }

        [Required]
        public bool Gender { get; set; }

        [Required]
        [StringLength(11)]
        public string Pesel { get; set; }
    }
}