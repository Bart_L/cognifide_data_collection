namespace cognifide_data_collection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateUsers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Jan', 'Kowalski', '1988-01-12', 1, 43298756710)");
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Andrzej', 'Krakowski', '1967-04-10', 1, 98712345678)");
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Kamila', 'Nowak', '1986-02-02', 0, 12342345678)");
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Joanna', 'Kwiatkowska', '1990-11-11', 0, 9042345678)");

        }

        public override void Down()
        {
        }
    }
}
