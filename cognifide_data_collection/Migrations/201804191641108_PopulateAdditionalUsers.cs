namespace cognifide_data_collection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateAdditionalUsers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Krzysztof', 'Go�kowski', '1983-01-12', 1, 09548756710)");
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Andrzej', 'Ka�u�ny', '1991-04-10', 1, 00712091238)");
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Agnieszka', 'Andrzejewska', '1955-02-02', 0, 63972345678)");
            Sql("INSERT INTO Users (Name, SecondName, Birthdate, Gender, Pesel) VALUES ('Karolina', 'Konarska', '1991-11-11', 0, 9309876718)");
        }
        
        public override void Down()
        {
        }
    }
}
