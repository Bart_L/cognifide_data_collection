﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cognifide_data_collection.Startup))]
namespace cognifide_data_collection
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
